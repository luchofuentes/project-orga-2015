#include <stdio.h>
#include <stdlib.h>
#include "lista.h"

tLista crearLista(){

    tLista lista;
    lista.size    = 0;
    lista.header  = NULL;
    lista.tail    = NULL;

    return lista;

}

void insertarAtras(tLista* L, tNodo* pos, tElemento x){

    if(pos==NULL && L->size==0)
        insertarPpio(L,x);

    else{
        tNodo * nuevoNodo = malloc(sizeof(tNodo));
        nuevoNodo->next   = pos->next;
        pos->next         = nuevoNodo;
        L->size++;
    }

}

void insertarPpio(tLista* L,tElemento x){

       tNodo * nNodo = malloc(sizeof(tNodo));
       nNodo->elem   = x;
       nNodo->next   = NULL;

       if (listaVacia(*L)){
            L->header = nNodo;
            L->tail   = nNodo;
       }
       else{
            nNodo->next = L->header;
            L->header = nNodo;
       }

       L->size++;

}

void eliminar(tLista* L, tNodo* pos){

    //Eliminar tiene dos casos especiales

    //El primer caso es que tenga un solo elemento y pos sea el header.
    if(L->header->next==NULL && L->header==pos){

        L->header = NULL;
        L->tail   = NULL;
        L->size--;
    }

    //El segundo caso es que se tenga que eliminar el todo header.
    else if(L->header==pos && L->size > 1){

        L->header = L->header->next;
        L->size--;
    }

    //Este el es el caso general.
    else{

        tNodo * nAuxAnterior  = L->header;
        tNodo * nAux          = L->header;

        while(nAux->next!=NULL){

            if(nAux == pos){

                nAuxAnterior->next = nAux->next;
                nAux->next         = NULL;
                L->size--;
                free(pos);
            }

            else{

                nAuxAnterior = nAux;
                nAux         = nAux->next;
            }
        }
    }
}

tNodo *siguiente(tNodo* pos){

    if(pos)
        return pos->next;

    return NULL;

}

tNodo *primera(tLista L){

    return L.header;

}

tNodo *ultima(tLista L){

    return L.tail;

}

tElemento *elemento(tNodo* pos){

    return &(pos->elem);

}

int listaVacia(tLista L){
    if (L.size==0)
        return 1;
    else
        return 0;
}
