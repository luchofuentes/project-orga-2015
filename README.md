# Project of C #

First project

### Contributors  ###

* Luciano Fuentes.
* Salvador Catalfamo.
* Leandro Furyk.

### Init repository ###
* git init
* git remote add origin https://luchofuentes@bitbucket.org/luchofuentes/project-orga.git
* git pull origin master

### Update or add some files ###
* git add nameoffile
* git commit -m 'Commit of update'
* git push -u origin master 

### Download update of repository ###
* git pull origin master
