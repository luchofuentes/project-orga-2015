#include <stdlib.h>
#include <stdio.h>
#include "memoria.h"


int inicializarMemoria(int max){

    if (bloquesLibres==NULL){


        bloquesLibres   = malloc(sizeof(tLista));
        bloquesOcupados = malloc(sizeof(tLista));
        *bloquesLibres  = crearLista();
        *bloquesLibres  = crearLista();

        if(bloquesLibres==NULL || bloquesOcupados==NULL)
            return 3; //En caso de que no se pudo crear algunas de las dos listas se retorna 3.

        int tamanio=0;
        while (tamanio < max)
            tamanio+=8;

        mem = malloc(sizeof(char)*tamanio);
        if (mem==NULL)
            return 1;

        //Creo el primer elemento de la lista de libres

        tElemento elem;
        elem.a = 0;
        elem.b = (tamanio-1);
        insertarPpio(bloquesLibres,elem);

        return 0;
    }
    else
        return 2;

    return 3; //Si llego ac� hubo cualquier otro error no contemplado anteriormente
}

void *asignar(int cant){

    int encontre =  0;
    int inicio   =  0;
    int fin      =  0;

    tNodo * pos = primera(*bloquesLibres);

    while (!encontre && pos != NULL){

        inicio = elemento(pos)->a;
        fin    = elemento(pos)->b;

        if ((fin-inicio+1) >= cant){

            //Manejo de la lista de ocupados
            tElemento nuevo;
            nuevo.a = elemento(pos)->a;
            nuevo.b = nuevo.a + cant-1;

            //Recorro la lista de ocupados para insertar ordenado
            tNodo * nAux         = primera(*bloquesOcupados);
            tNodo * nAuxAnterior = primera(*bloquesOcupados);

            //busco la posicion en la que tengo que insertar el elemento

            while(nAux !=NULL &&  nuevo.a > elemento(nAux)->b ){
                nAuxAnterior = nAux;
                nAux         = siguiente(nAux);;

            }
            if (nAux == primera(*bloquesOcupados))

                insertarPpio(bloquesOcupados,nuevo);

            else

                insertarAtras(bloquesOcupados,nAuxAnterior,nuevo);

            //Manejo de la lista de libres
            elemento(pos)->a+=cant;
            encontre=1;
        }

        else{
            pos = siguiente(pos);
        }

    }

    if(pos==NULL)
        return NULL;

    return &mem[inicio];
}


void liberar(void * bloque){

    tNodo * posOc     = primera(*bloquesOcupados);
    int encontreEnOcu = 0;

    while (!encontreEnOcu && posOc!=NULL){

        if (&mem[posOc->elem.a] == bloque){

            tNodo * nAux         = primera(*bloquesLibres);
            tNodo * nAuxAnterior = primera(*bloquesLibres);

            //busco la posicion en la que tengo que insertar el elemento
            while(nAux!=NULL && (elemento(posOc)->b > elemento(nAux)->a)){
                nAuxAnterior = nAux;
                nAux         = siguiente(nAux);;
            }

            if (nAux ==bloquesLibres->header)
                insertarPpio(bloquesLibres,*elemento(posOc));

            else
                insertarAtras(bloquesLibres,nAuxAnterior,*elemento(posOc));

            //elimino de la lista de ocupados,tengo que eliminar despues
            eliminar(bloquesOcupados,posOc);
            encontreEnOcu=1;
        }
        else{
            posOc= siguiente(posOc);
        }

    }

}
