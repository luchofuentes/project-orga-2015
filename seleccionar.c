#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "pila.h"
#include "memoria.h"


int modulo(int elem, int mod){
    return (elem%mod)==0;
}

int mayor (int elem, int may){
    return elem>may;
}

int menor (int elem, int men){
    return elem<men;
}

void help(){
    printf("Usage : seleccionar [-h] [<criterio> <archivo entrada>] [<archivo salida>]\n");
}

int main(int argc, char ** arg){


    if(argc < 5 && argc > 1){

        if(strcmp(arg[1],"-h")==0){
            help();
            exit(0);
        }
        else if(arg[1][0]=='%' || arg[1][0]=='>' || arg[1][0]=='<'){

            char cnumero[100];
            int i       = 1;
            int inumero = 0;

            while(arg[1][i]!='\0'){
                cnumero[i-1] = arg[1][i];
                i++;
            }
            cnumero[i-1] = '\0';
            inumero      = atoi(cnumero);

            FILE * entrada = NULL;
            if(arg[2]!=NULL){
                entrada =  fopen(arg[2],"r");
                if(entrada==NULL){
                    printf("Error con el archivo de entrada\n");

                    exit(1);
                }
            }
            else{
                printf("Error, no se espcifico el archivo de entrada\n");
                help();
                exit(1);
            }

            FILE * salida  = NULL;
            if(arg[3]!=NULL){
                salida = fopen(arg[3],"w");
                if(salida==NULL){
                    printf("Error con el archivo de salida\n");
                    exit(1);
                }
            }
            else
                salida = stdout;

            inicializarMemoria(1024*5);
            tPila * pila = crearPila();
            char  buffer[100];

            while(fgets(buffer,100,entrada)!=NULL){
                apilar(atoi(buffer),&pila);
            }

            fclose(entrada);
            tPila * subP;

            switch(arg[1][0]){

                case '%':

                    subP = subPila(modulo,inumero,pila);
                    while(pilaVacia(subP)){
                        fprintf(salida,"%d ",desapilar(&subP));
                    }
                    printf("\n");
                    break;

                case '>':

                    subP = subPila(mayor,inumero,pila);
                    while(pilaVacia(subP)){
                        fprintf(salida,"%d ",desapilar(&subP));
                    }
                    printf("\n");
                    break;

                case '<':

                    subP = subPila(menor,inumero,pila);
                    while(pilaVacia(subP)){
                        fprintf(salida,"%d ",desapilar(&subP));
                    }
                    printf("\n");
                    break;
            }

            fclose(salida);
        }

        else{
            printf("Fail usage!\n");
            help();
            exit(1);
        }

    }
    else{
        printf("Fail usage!\n");
        help();
        exit(1);
    }

    return 0;
}
