#ifndef LISTA_H_INCLUDED
#define LISTA_H_INCLUDED

typedef struct Elemento {
    int a;
    int b;
}tElemento;

typedef struct Nodo {
    tElemento elem;
    struct Nodo * next;
}tNodo;

typedef struct Lista {
    tNodo* header; //puntero al primer nodo de la lista
    tNodo* tail; //puntero al último nodo de la lista
    int size;
}tLista;

/* Crea una lista vacía y la devuelve. */
tLista      crearLista();

/* Inserta el elemento 'x' en la posición
 * inmediata siguiente a 'pos' en la lista 'L'. */
void        insertarAtras(tLista* L, tNodo* pos, tElemento x);

/* Inserta el elemento 'x' en la primera posición
 * de la lista 'L'. */
void        insertarPpio(tLista* L,tElemento x);

/* Elimina el elemento de la posición 'pos'
 * de la lista 'L'. */
void        eliminar(tLista* L, tNodo* pos);

/* Devuelve la posición siguiente a la posicion 'pos'.
 * Devuelve NULL si p es NULL o si no existe
 * posición siguiente. */
tNodo*      siguiente(tNodo* pos);

/* Devuelve la primera posición de la lista 'L'. */
tNodo*      primera(tLista L);

/* Devuelve la ultima posición de la lista 'L'. */
tNodo*      ultima(tLista L);

/* Devuelve un puntero al elemento que ocupa la
 * posición pos de la lista. */
tElemento*  elemento(tNodo* pos);

/* Devuelve verdadero (!=0) si la lista 'L' está vacía,
 * y falso (0) si la lista contiene al menos un elemento. */
int         listaVacia(tLista L);


#endif // LISTA_H_INCLUDED
