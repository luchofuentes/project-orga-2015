.TH seleccionar 7 "14 Octubre 2015" "version 0.3"
.SH NAME
seleccionar \- Ejemplo para utilizar una memoria dinamica
.SH SYNOPSIS
seleccionar [-h] [<criterio> <archivo entrada>] [<archivo salida>]
.SH DESCRIPTION
seleccionar es parte del primer proyecto de Organización de Computadoras 2015, que brinda un ejemplo de como funciona la memoria dinamica en c, y ademas brinda informacion de como utilizar TDA en c. El ejemplo se basa en almacenar en una pila de enteros, numeros que se encuentran en el archivo de entrada, y teniendo en cuenta el criterio de selección, escriba en pantalla, o en caso de que se especifique, en el archivo de salida, los enteros que cumplan con el criterio de selección.
.PP
Atencion!! para este proyecto la memoria dinamica usada toma como tamaño 5 kilobytes!
.PP
La pila utiliza una memoria dinamica y sus respectivas operaciones para el manejo de
su estructura.
.PP
La estructura de memoria dinamica usada en este proyecto es manejada por dos estructu
ras auxiliares, dichas estructuras son dos listas simples enlazadas, sin centinela. E
stas dos estructuras manejan los bloques libres y los bloques ocupados de la memoria dinamica. A su vez la memoria dinamica tiene un arreglo de bytes que es ahí donde alm
acena la información.
.PP
La listas utilizadas por la memoria dinamica estan compuestas por nodos, donde cada n
odo tiene un elmento y una referencia al siguiente nodo.
.SH OPTIONS
.IP -h
Muestra como se debe usar el programa
.IP <criterio>
El criterio puede ser, '<N', '>N', '%N', siendo N un entero.
.IP "<archivo de entrada>"
Archivo donde contiene los enteros a seleccionar, los enteros tienen que estar separados por un '\n', por ende en cada linea del archivo tiene que haber como maximo un entero.
.IP "<archivo de salida>"
Archivo donde se escribiran los enteros que cumplan con el criterio de seleccion, en caso de no especificar se escribira en pantalla (stdout).
.SH ERROR
En caso de no especificar parámetros, especificar parámetros inválidos, se mostrar en pantalla (stdout) como se debe usar el programa.
.SH EXAMPLE
Suponiendo que se tiene un archivo 'numeros.txt' con los siguientes numeros: 
.PP
1
.br
5
.br
6
.br
3
.br
25
.br
63
.br
12
.br
11
.br
21
.PP
$ seleccionar %3 numeros.txt 
.br
6 3 63 12 21
.br
$ seleccionar ">20" numeros.txt
.br
25 63 21
.br
$ seleccionar "<20" numeros.txt
.br
1 5 6 3 12 11
.SH LICENSE
seleccionar es distribuido bajo los términos de GNU General Public License, versión 2 o posterior.
.SH AUTHOR
Luciano Fuentes (lucho.fuentez@gmail.com)
.br
Leandro Furyk (leandrofuryk@gmail.com)
.br
Salvador Catalfamo
