#include <stdio.h>
#include <stdlib.h>
#include "pila.h"
#include "memoria.h"
#include <limits.h>

tPila* crearPila(){

    tPila *pila= NULL;
    return pila;
}

int tope(tPila * P){

   if(P==NULL)
    return INT_MAX; //Utilizaremos el integer Maximo para informar de una pila vacia.
  else
    return P->elem;
}

int desapilar(tPila ** P){

    if(*P==NULL)
        return -1;

    else{
        int n;
        n=(*P)->elem;

        tPila* aux= (*P); //Guarda el primer nodo de la pila en un puntero para luego ser liberado

        tPila* pnext=(*P)->next;
        *P=pnext; //El segundo elemento de la pila se convierte en el tope

        aux->next=NULL; //Se libera el espacio en la heap del nodo a desapilar
        liberar(aux);

        return n;
    }
}

int apilar(int a, tPila** P){

    tPila* p1= asignar(sizeof(tPila));
    if(p1==NULL) //Si la reserva de memoria no fue exitosa retorna 1.
        return 1;
    p1->elem=a;
    p1->next=*P;	//Se inserta el nuevo elemento en el tope de la pila
    (*P)=p1;
    return 0;
}

int pilaVacia(tPila * P){

    if(P==NULL)
        return 0; //Si el puntero no tiene ninguna referencia, la pila esta vacia y retorna cero
    else
        return 1;
}

tPila* subPila(int (*f)(int,int), int a, tPila* P){

    tPila* aux   = crearPila();
    int encontre = 1;       //La condicion encontre se pone en falso, si no hay mas elementos en la pila
    while(encontre==1){

       int n = f(P->elem,a); //Se le asigna a 'n' 1 en verdadero, 0 en falso, si cumple con la condicion

       if(n!=0)
            apilar(P->elem,&aux); //Si es verdadero, se apila el elemento en la nueva pila

       if(P->next==NULL)
            encontre=0;
       else
            P=P->next;		//Se avanza al proximo elemento de la pila
    }
    return aux;
}

