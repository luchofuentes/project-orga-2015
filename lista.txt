NAME
============================================
crearLista, insertarAtras, insertarPpio, eliminar, siguiente, primera, ultima, elemento, listaVacia - Lista simple enlazada sin centinela utilizada para manejar una memoria dinamica.

SYPNOPSIS
============================================

    #include "lista.h"
    
    typedef struct Elemento{
        int a;
        int b;
    }tElemento;

    typedef struct Nodo{
        tElemento elem:
        struct Nodo *next;
    }tNodo;

    typedef struct Lista{
        tNodo *header;
        tNodo *tail;
        int size;
    }

    tLista     crearLista();
    void       insertarAtras(tLista *L, tNodo *pos, tElemento x);
    void       insertarPpio(tLista *L, tElemento x);
    void       eliminar(tLista *L, tNodo *pos);
    tNodo     *siguiente(tNodo *pos);
    tNodo     *primera(tLista L);
    tNodo     *ultima(tLista L);
    tElemento *elemento(tNodo *pos);
    int        listaVacia(tLista L);

DESCRIPTION
============================================
    crearLista() retorna una lista vacia nueva, con size=0 y los nodos header y tail como NULL.

    insertarAtras(tLista *L, tNodo *pos, tElemento x) inserta el elemento 'x' en la posición inmediata siguiente a 'pos' en la lista 'L'. En caso de que 'pos' sea NULL el elemento si inserta al principio.

    insertarPpio(tLista *L, tElemento x) Inserta el elemento 'x' en la primera posición de la lista 'L'.

    eliminar(tLista *L, tNodo *pos) Elimina el elemento de la posición 'pos' de la lista 'L'.

    siguiente(tNodo *pos) Retorna un puntero a la siguiente posición de 'pos'. En caso de que pos sea NULL, se va a retornar NULL.

    primera(tLista L) Retorna un puntero a la primera posición de la lista 'L'.

    ultima(tLista L) Retorna un puntero a la ultima posición de la lista 'L'.

    elemento(tNodo *pos) Retorna un puntero al elemento que ocupa la posición 'pos' de la lista.

    listaVacia(tLista L) Retorna verdadero(!=0) si la lista está vacía, y falso(0) si la lista contiene al menos un elemento.

LICENSE
============================================
    lista.c y lista.h es distribuido bajo los términos de GNU General Public License, versión 2 o posterior.

AUTHOR
============================================
    Luciano Fuentes (lucho.fuentez@gmail.com).
    Leandro Furyk (leandrofuryk@gmail.com).
    Salvador Catalfamo
