#ifndef MEMORIA_H_INCLUDED
#define MEMORIA_H_INCLUDED
#include "lista.h"

tLista * bloquesLibres;
tLista * bloquesOcupados;
char   * mem;

/* Crea e inicializa un espacio de memoria de m bytes, siendo
 * m el menor múltiplo de 8 mayor o igual que max. Esta función
 * debe inicializar las listas de bloques libres de forma tal que
 * toda la memoria esté disponible. Retorna 0 si la creación fue exitosa, (!=0)
 * en caso de error, según el siguiente código de error: 1 no hay suficiente memoria, 2 la memoria
 * ya fue inicializada, 3 cualquier otro error. */
int     inicializarMemoria(int max);

/* Reserva cant bytes de memoria contigua y devuelve el puntero al
 * primer byte. Retorna NULL en caso de no poder encontrar cant bytes contiguos. */
void*   asignar(int cant);

/* Libera el bloque de memoria apuntado por bloque y actualiza
 * las estructuras auxiliares. */
void    liberar(void * bloque);

#endif // MEMORIA_H_INCLUDED
